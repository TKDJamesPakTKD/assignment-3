import javax.swing.JOptionPane;

/**
User Authentication using JOptionPane.
This application prompts the user for user name and password. 

Author: James Pak
CSC130 Assignment 3 Part 1
Professor Kanchanawanchai
June 3, 2014
*/

public class Assignment3
{
  
  public static void main(String[] args) {
    // TODO Auto-generated method stub
    final int TRIAL_LIMIT = 3;
    String correctusername = "james";
    String correctpassword = "james";
    String correctType = "Student";
    String username, password;
    int trialscount = 0; //1. Initialize
    JOptionPane.showMessageDialog(null, "User Authentication");
    while (trialscount < TRIAL_LIMIT) {
      username = JOptionPane.showInputDialog("Enter User Name(james)");
      if (username.equalsIgnoreCase(correctusername)) {
        do {
          password = JOptionPane.showInputDialog("Enter Password(james)");
          if (password.equals(correctpassword)) {
          JOptionPane.showMessageDialog(null, "Welcome " + username + "!"); 

          String[] choices = { "Admin", "Student", "Staff" };
          String input = (String) JOptionPane
              .showInputDialog(null, "Choose account type...",
                  "Account Type", JOptionPane.QUESTION_MESSAGE,
                  null, choices, choices[1]);
          //loop until acccount type is correct
          while (!input.equalsIgnoreCase(correctType)) {
            input = (String) JOptionPane.showInputDialog(null,
                "Choose account type...", "Account Type",
                JOptionPane.QUESTION_MESSAGE, null, choices,
                choices[1]);
          }
          
          switch (input) {
            case "Admin":
              JOptionPane.showMessageDialog(null, "Welcome Admin");
              break;
            case "Student":
              JOptionPane.showMessageDialog(null,  "Welcome Student");
              break;
            case "Staff":
              JOptionPane.showMessageDialog(null, "Welcome Staff");
              break;
          }
          
          System.exit (0);
          //NOTE: Program ends once account type is correct!
        } else {  
            trialscount++;
            JOptionPane.showMessageDialog(null, "Incorrect Password. " + trialscount + " of 3 tries."); 
         }
        }
          while(trialscount < TRIAL_LIMIT);
    } else { 
        trialscount++;
        JOptionPane.showMessageDialog(null, "Invalid Username. Used " + trialscount + " of 3 tries.");
      }
    }
    JOptionPane.showMessageDialog(null, "Contact Administrator for assistance");
    System.exit (0);
  }     
}
